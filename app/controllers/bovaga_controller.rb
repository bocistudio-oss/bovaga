# frozen_string_literal: true

# All Bovaga controllers are inherited from here.
class BovagaController < Bovaga.parent_controller.constantize
  skip_before_action :verify_authenticity_token
  before_action :whitelist_remote_ip!
  before_action :bovaga_authorize!

  if respond_to?(:helper_method)
    helpers = %w(bovaga_mapping)
    helper_method(*helpers)
  end

  protected

  def whitelist_remote_ip!
    return if request.local?

    bank_adapter = bovaga_mapping.bank_adapter_klass.constantize
    return if bank_adapter.whitelisted_ips.include?(request.remote_ip)

    render json: { error: I18n.t(:forbidden_access, scope: 'errors.messages') }, status: :forbidden
  end

  # TODO: Find another way
  def bovaga_authorize!
    if defined?(Doorkeeper)
      doorkeeper_authorize!
    else
      raise "Bovaga could not find the `Doorkeeper`.\n" + \
        "Please use Doorkeeper or implement your own authentication method."
    end
  end

  # Proxy to bovaga map name
  def resource_name
    bovaga_mapping.name
  end
  alias :scope_name :resource_name

  # Proxy to devise map class
  def resource_class
    bovaga_mapping.to
  end

  def bovaga_mapping
    @bovaga_mapping ||= request.env["bovaga.mapping"]
  end
end

