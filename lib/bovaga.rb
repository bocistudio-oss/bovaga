# frozen_string_literal: true

require "bovaga/railtie"

module Bovaga
  module Controllers
    autoload :RequestHistoryConcern, 'bovaga/controllers/request_history_concern'
  end

  module BankAdapter
    autoload :Bca, 'bovaga/bank_adapter/bca'
  end

  # When false, Bovaga will not attempt to reload routes on eager load.
  # This can reduce the time taken to boot the app but if your application
  # requires the Bovaga mappings to be loaded during boot time the application
  # won't boot properly.
  mattr_accessor :reload_routes
  @@reload_routes = true

  # The parent controller all Devise controllers inherits from.
  # Defaults to ApplicationController. This should be set early
  # in the initialization process and should be set to a string.
  mattr_accessor :parent_controller
  @@parent_controller = "ApplicationController"

  # Define whitelisted ip addresses
  mattr_accessor :whitelisted_ips
  @@whitelisted_ips = []

  # PRIVATE CONFIGURATION

  # Store scopes mappings
  mattr_reader :mappings
  @@mappings = {}

  # Define a set of modules that are called when a mapping is added.
  mattr_reader :helpers
  @@helpers = Set.new
  # @@helpers << Bovaga::Controllers::Helpers

  # Define supported bank_codes
  mattr_reader :bank_codes
  @@bank_codes = %w(bca)

  # Define default currency
  mattr_reader :default_currency
  @@default_currency = 'IDR'

  def self.setup
    yield self
  end

  class Getter
    def initialize(name)
      @name = name
    end

    def get
      ActiveSupport::Dependencies.constantize(@name)
    end
  end

  def self.ref(arg)
    ActiveSupport::Dependencies.reference(arg)
    Getter.new(arg)
  end

  def self.add_mapping(resource, options)
    mapping = Bovaga::Mapping.new(resource, options)
    @@mappings[mapping.name] = mapping
    @@default_scope ||= mapping.name
    @@helpers.each { |h| h.define_helpers(mapping) }
    mapping
  end

  def self.env
    Rails.env if defined?(Rails)
  end
end

require 'bovaga/mapping'
require 'bovaga/rails'
