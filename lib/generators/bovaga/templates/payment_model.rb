# frozen_string_literal: true

class <%= singular_name.classify %>Payment < Bovaga::<%= options.bank_code.downcase.classify %>::Payment
  def va_prefix_code
    # Write your prefix code here..

    # NOTE: Bovaga expect string as return value
  end

  def va_type
    # Write the type of current configuration..
    # accepted_value:
    # for 'BCA' either :no_bill or :fixed_amount

    # NOTE: Bovaga expect symbol as return value
  end

  def va_name
    # Write your customer name here..

    # NOTE: Bovaga expect string as return value
  end

  def va_total_amount
    # Write your code here..
    # This will be a total amount that need to be paid, or
    # leave it '0' (zero) when using ewallet-ish

    # NOTE: Bovaga expect numeric as return value
  end

  def va_currency
    # Write currency code here..

    # NOTE: Bovaga expect string as return value
  end

  def va_bill_valid?
    # Write your code here..
    # Check if bill is still valid to be paid or not

    # NOTE: Bovaga expect boolean as return value
  end

  # NOTE: Not used yet
  def va_items
    # Write your code here..
    # This should be list of bill's items, or
    # leave it [] (empty array)

    # NOTE: Bovaga expect array as return value
  end
end
