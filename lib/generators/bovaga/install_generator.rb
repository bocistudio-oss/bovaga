# frozen_string_literal: true

require_relative 'migration_generator'

module Bovaga
  module Generators
    class InstallGenerator < MigrationGenerator
      source_root File.expand_path("../templates", __FILE__)

      desc "Creates a Bovaga initializer and copy locale files to your application."

      class_option :skip_history, type: :boolean, default: false, desc: "Skip to save history (default: false)"

      def copy_initializer
        template "initializer.rb", "config/initializers/bovaga.rb"
      end

      def copy_locale
        copy_file "../../../../config/locales/en.yml", "config/locales/bovaga.en.yml"
        copy_file "../../../../config/locales/id.yml", "config/locales/bovaga.id.yml"
      end

      def create_request_histories
        add_bovaga_migration("create_bovaga_request_histories") unless options.skip_history
      end
    end
  end
end
