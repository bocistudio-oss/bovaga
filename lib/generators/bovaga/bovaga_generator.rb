# frozen_string_literal: true

require 'rails/generators/named_base'

module Bovaga
  module Generators
    class BovagaGenerator < Rails::Generators::NamedBase
      namespace "bovaga"

      source_root File.expand_path("../templates", __FILE__)

      desc "Generate all necessary models, controllers and route for Bovaga."

      class_option :bank_code, desc: "Define bank code", type: :string, required: true, enum: %w(bca)

      def generate_model
        template "bill_model.rb", "app/models/#{singular_name}_bill.rb"
        template "payment_model.rb", "app/models/#{singular_name}_payment.rb"
      end

      def create_controller
        template "controller.rb", File.join("app/controllers", class_path, "#{file_name}_controller.rb")
      end

      def add_bovaga_routes
        bovaga_route  = "bovaga_for :#{singular_name}".dup
        bovaga_route << %Q(, bank: :#{options.bank_code.downcase})
        bovaga_route << %Q(, controller: '#{class_name.underscore}')
        route bovaga_route
      end

      private

      def file_name
        @_file_name ||= remove_possible_suffix(super)
      end

      def remove_possible_suffix(name)
        name.sub(/_?controller$/i, "")
      end
    end
  end
end
