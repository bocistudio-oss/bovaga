# frozen_string_literal: true

module Bovaga
  module BankAdapter
    module Bca
      def self.whitelisted_ips
        ips = if Bovaga.env.try(:production?)
          %w(202.6.211.92 202.6.208.92)
        else
          %w(202.6.215.234)
        end

        Bovaga.whitelisted_ips + ips
      end
    end
  end
end
