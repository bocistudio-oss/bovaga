# frozen_string_literal: true

require "active_support/core_ext/object/try"
require "active_support/core_ext/hash/slice"

module ActionDispatch::Routing
  class Mapper
    def bovaga_for(*resources)
      options = resources.extract_options!
      resources.map!(&:to_sym)

      resources.each do |resource|
        mapping = Bovaga.add_mapping(resource, options)

        bovaga_scope mapping.name do
          with_bovaga_exclusive_scope mapping.fullpath, mapping.name, options do
            bovaga_bill(mapping)
            bovaga_payment(mapping)
          end
        end
      end
    end

    def bovaga_scope(scope)
      constraint = lambda do |request|
        request.env["bovaga.mapping"] = Bovaga.mappings[scope]
        true
      end

      constraints(constraint) do
        yield
      end
    end
    alias :as :bovaga_scope

    protected

    def bovaga_bill(mapping)
      post :bill, controller: mapping.capture_controller, action: 'bill'
    end

    def bovaga_payment(mapping)
      post :payment, controller: mapping.capture_controller, action: 'payment'
    end

    def with_bovaga_exclusive_scope(new_path, new_as, options) #:nodoc:
      current_scope = @scope.dup

      exclusive = { as: new_as, path: new_path }
      exclusive.merge!(options.slice(:constraints, :defaults, :options))

      if @scope.respond_to? :new
        @scope = @scope.new exclusive
      else
        exclusive.each_pair { |key, value| @scope[key] = value }
      end
      yield
    ensure
      @scope = current_scope
    end
  end
end
