# frozen_string_literal: true

module Bovaga
  module Bca
    class BillResponse
      attr_accessor :status, :bill

      def initialize(bill)
        self.bill = bill

        # Status default to failure with :invalid key
        self.status = {
          type: '01',
          key: :invalid
        }
      end

      def body
        {
          'CompanyCode' => company_code,
          'CustomerNumber' => customer_number,
          'RequestID' => request_id,
          'InquiryStatus' => status_type,
          'InquiryReason' => {
            'Indonesian' => I18n.t(status_key, scope: status_scope, locale: :id),
            'English' => I18n.t(status_key, scope: status_scope, locale: :en)
          },
          'CustomerName' => customer_name,
          'CurrencyCode' => currency_code,
          'TotalAmount' => total_amount,
          'SubCompany' => resource_subcompany,
          'DetailBills' => bill_details,
          'FreeTexts' => [],
          'AdditionalData' => ''
        }
      end

      def success!
        self.status = {
          type: '00',
          key: :success
        }
      end

      def error!(key)
        self.status = {
          type: '01',
          key: key
        }
      end

      def process(&block)
        if bill.request_parameter_valid?
          if bill.active?
            success!
            yield
          else
            error!(:timeout)
          end
        end
      end

      private

      # NOTE: This value is from request parameter
      def company_code
        bill.try(:company_code)
      end

      # NOTE: This value is from request parameter
      def customer_number
        bill.try(:customer_number)
      end

      # NOTE: This value is from request parameter
      def request_id
        bill.try(:request_id)
      end

      def status_type
        status.try(:[], :type)
      end

      def status_key
        status.try(:[], :key)
      end

      def status_scope
        'bca.bill.messages'
      end

      # NOTE: This value is from main app
      def customer_name
        bill.try(:va_name) || ''
      end

      # NOTE: This value is from bill model
      def currency_code
        bill.try(:va_currency) || Bovaga.default_currency
      end

      # NOTE: This value is from main app
      def total_amount
        "%.2f" % (bill.try(:va_total_amount) || 0)
      end

      # NOTE: This value is from main app
      def resource_subcompany
        "%05d" % (bill.try(:va_subcompany) || 0)
      end

      # TODO: Use va_items to populate bill_details
      def bill_details
        []
      end
    end
  end
end
