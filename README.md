# Bovaga
This application is an attempt to create Virtual Account Gateway for Indonesian bank without third party.

## Getting Started

Add the following line to your Gemfile:
```ruby
gem 'bovaga', :git => 'https://gitlab.com/agustinus.pratama/bovaga.git', :branch => 'master'
```

Then run `bundle install`

Next, you need to initialize bovaga installer:

```
$ rails generate bovaga:install [--skip-history]
```

There will be a migration to generate RequestHistory table, when there are no skip-history.

Update the database by run `rails db:migrate`

Setup the model for configure with:

```
$ rails generate bovaga NAME --bank-code=<bank>
```

Replace `NAME` with the class name to handle this type and `<bank>` with available supported banks.


## Supported banks

### BCA

There are 2 types:

* __no_bill__
  Use this when using ewallet-ish (customer just add the funds without specify amount on our part)

* __fixed_amount__
  Use this when using bill (customer pay for specific amount that should match with our part)

## Contributing
Currently its only for Bocistudio developer.

## License
The gem is available as open source under the terms of the [MIT License](https://opensource.org/licenses/MIT).
